package main

import (
	"io"
	"os"
	"sync"
	"tunme/internal/utils"
	"tunme/pkg/tunme"
	"tunme/pkg/tunme_app"
)

// must be the same on the client & the server
const buffSize = 4096

func stdinToStream(stream tunme.Stream) {

	buff := make([]byte, buffSize)

	for {
		n, readErr := os.Stdin.Read(buff[2:])

		if n > 0 {

			buff[0] = byte(n >> 8)
			buff[1] = byte(n)

			_, writeErr := stream.Write(buff[:2+n])
			if writeErr != nil {
				panic(writeErr)
			}

			// TODO: flush less frequently if not on a TTY
			flushErr := stream.Flush()
			if flushErr != nil {
				panic(flushErr)
			}
		}

		if readErr == io.EOF {
			break
		} else if readErr != nil {
			panic(readErr)
		}
	}

	// one-side EOF marker
	buff[0] = 0
	buff[1] = 0

	_, err := stream.Write(buff[:2])
	if err != nil {
		panic(err)
	}

	err = stream.Flush()
	if err != nil {
		panic(err)
	}
}

func streamToStdout(stream tunme.Stream) {

	buff := make([]byte, buffSize)

	for {
		ok, err := utils.ReadBytes(stream, buff[:2])
		if !ok {
			panic(err)
		} else if err == io.EOF {
			panic("missing EOF marker")
		}

		chunkLen := (int(buff[0]) << 8) | int(buff[1])

		if chunkLen == 0 {
			// one-side EOF marker
			break
		}

		if chunkLen > buffSize {
			panic("invalid chunk size")
		}

		ok, readErr := utils.ReadBytes(stream, buff[:chunkLen])
		if !ok {
			panic(readErr)
		}

		_, writeErr := os.Stdout.Write(buff[:chunkLen])
		if writeErr != nil {
			panic(writeErr)
		}
	}
}

func send(link tunme.Link) {

	stream, err := link.OpenStream()
	if err != nil {
		panic(err)
	}
	defer stream.Close()

	stdinToStream(stream)
}

func receive(link tunme.Link) {

	stream, err := link.AcceptStream()
	if err != nil {
		panic(err)
	}
	defer stream.Close()

	streamToStdout(stream)
}

func main() {

	var app tunme_app.App

	link, err := app.CreateLink(os.Args[1], os.Args[2:])
	if err != nil {
		panic(err)
	}
	defer link.Close()

	var waitGroup sync.WaitGroup
	defer waitGroup.Wait()

	waitGroup.Add(1)
	go func() {
		send(link)
		waitGroup.Done()
	}()

	receive(link)
}

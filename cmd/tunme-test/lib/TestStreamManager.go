package lib

import "tunme/pkg/tunme"

type TestStreamManager struct {
	link    tunme.Link
	streams map[uint64]*TestStream
}

func createTestStreamManager() *TestStreamManager {
	return &TestStreamManager{
		streams: make(map[uint64]*TestStream),
	}
}

func (mgr *TestStreamManager) CreateTestStream(stream1 tunme.Stream, stream2 tunme.Stream) *TestStream {

	testStream := createTestStream(stream1, stream2)

	mgr.streams[stream1.GetId()] = testStream
	mgr.streams[stream2.GetId()] = testStream

	return testStream
}

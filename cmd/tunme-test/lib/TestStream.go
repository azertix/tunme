package lib

import (
	"bytes"
	"math/rand"
	"tunme/internal/utils"
	"tunme/pkg/tunme"
)

type TestStream struct {
	stream1     tunme.Stream
	sendRng1    *rand.Rand
	receiveRng1 *rand.Rand
	stream2     tunme.Stream
	sendRng2    *rand.Rand
	receiveRng2 *rand.Rand
}

func createTestStream(stream1 tunme.Stream, stream2 tunme.Stream) *TestStream {
	return &TestStream{
		stream1:     stream1,
		sendRng1:    rand.New(rand.NewSource(int64(stream1.GetId()))),
		receiveRng1: rand.New(rand.NewSource(int64(stream1.GetId()))),
		stream2:     stream2,
		sendRng2:    rand.New(rand.NewSource(int64(stream2.GetId()))),
		receiveRng2: rand.New(rand.NewSource(int64(stream2.GetId()))),
	}
}

func (stream *TestStream) Send(n int) {

	sendBuff := make([]byte, n)
	stream.sendRng1.Read(sendBuff)

	_, err := stream.stream1.Write(sendBuff)
	if err != nil {
		panic(err)
	}

	err = stream.stream1.Flush()
	if err != nil {
		panic(err)
	}

	receiveBuff := make([]byte, n)
	stream.sendRng1.Read(receiveBuff)

	ok, err := utils.ReadBytes(stream.stream2, receiveBuff)
	if !ok {
		panic(err)
	}

	if bytes.Compare(sendBuff, receiveBuff) != 0 {
		panic("received data does not match what was sent")
	}
}

func (stream *TestStream) Receive(n int) {

	sendBuff := make([]byte, n)
	stream.sendRng1.Read(sendBuff)

	_, err := stream.stream2.Write(sendBuff)
	if err != nil {
		panic(err)
	}

	err = stream.stream2.Flush()
	if err != nil {
		panic(err)
	}

	receiveBuff := make([]byte, n)
	_, err = stream.stream1.Read(receiveBuff)
	if err != nil {
		panic(err)
	}

	if bytes.Compare(sendBuff, receiveBuff) != 0 {
		panic("received data does nat match what was sent")
	}
}

func (stream *TestStream) Close() {

	err := stream.stream1.Close()
	if err != nil {
		panic(err)
	}

	// TODO: the other end should do the same with stream2
}

func (stream *TestStream) received(receivingStream tunme.Stream, segment []byte) {

	expected := make([]byte, len(segment))

	if receivingStream == stream.stream1 {
		stream.receiveRng2.Read(expected)
	} else {
		stream.receiveRng1.Read(expected)
	}

	if bytes.Compare(expected, segment) != 0 {
		panic("received data does not match what has been sent")
	}
}

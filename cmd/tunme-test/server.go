package main

import (
	"io"
	"sync"
	"tunme/pkg/tunme"
)

func forwardStream(stream1 tunme.Stream, stream2 tunme.Stream, waitGroup *sync.WaitGroup) {

	for {
		buff := make([]byte, 4096)
		n, readErr := stream1.Read(buff)

		var writeErr error
		if n != 0 {
			_, writeErr = stream2.Write(buff[:n])
			if writeErr == nil {
				writeErr = stream2.Flush()
			}
		}

		if readErr == io.EOF {
			break
		}

		if readErr != nil {
			panic(readErr)
		}

		if writeErr != nil {
			panic(writeErr)
		}
	}

	err := stream1.Close()
	if err != nil {
		panic(err)
	}

	waitGroup.Done()
}

func runServer(link tunme.Link) {

	// for every stream opened by the peer, we open another one in the other direction

	var waitGroup sync.WaitGroup

	for {

		stream, err := link.AcceptStream()
		if err != nil {
			break
		}

		stream2, err := link.OpenStream()
		if err != nil {
			panic(err)
		}

		waitGroup.Add(2)
		go forwardStream(stream, stream2, &waitGroup)
		go forwardStream(stream2, stream, &waitGroup)
	}

	waitGroup.Wait()

	err := link.Close()
	if err != nil {
		panic(err)
	}
}

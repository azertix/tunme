package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"sync"
)

type PingResponder interface {
	io.ReadWriteCloser
}

type _pingResponder struct {
	_chan       chan *_icmpEchoRequest
	_closeMutex sync.Mutex
	_isClosed   bool
}

func CreatePingResponder() PingResponder {
	return &_pingResponder{
		_chan: make(chan *_icmpEchoRequest),
	}
}

type _icmpEchoRequest struct {
	SourceAddress      uint32
	DestinationAddress uint32
	Identifier         uint16
	SequenceNumber     uint16
	Payload            []byte
}

type _ipv4Header struct {
	VersionAndIhl          uint8
	Tos                    uint8
	TotalLength            uint16
	Identification         uint16
	FlagsAndFragmentOffset uint16
	Ttl                    uint8
	Protocol               uint8
	HeaderChecksum         uint16
	SourceAddress          uint32
	DestinationAddress     uint32
}

type _icmpEchoHeader struct {
	Type           uint8
	Code           uint8
	Checksum       uint16
	Identifier     uint16
	SequenceNumber uint16
}

type _ipv4AndIcmpEchoHeader struct {
	Ipv4     _ipv4Header
	IcmpEcho _icmpEchoHeader
}

type _rfc1071ChecksumCalculator struct {
	sum uint32
}

func (cks *_rfc1071ChecksumCalculator) Add(word uint16) {
	cks.sum += uint32(word)
}

func (cks _rfc1071ChecksumCalculator) Compute() uint16 {
	result := cks.sum
	for result >= 0x10000 {
		carry := result >> 16
		result &= 0xffff
		result += carry
	}
	return ^uint16(result)
}

func updateIpv4HeaderChecksum(header *_ipv4Header) {

	checksum := _rfc1071ChecksumCalculator{}

	header.HeaderChecksum = 0

	writer := bytes.Buffer{}
	binary.Write(&writer, binary.BigEndian, header)
	buff := writer.Bytes()
	for i := 0; i < len(buff); i += 2 {
		checksum.Add(binary.BigEndian.Uint16(buff[i : i+2]))
	}

	header.HeaderChecksum = checksum.Compute()
}

func updateIcmpChecksum(header *_icmpEchoHeader, payload []byte) {

	checksum := _rfc1071ChecksumCalculator{}

	header.Checksum = 0

	writer := bytes.Buffer{}
	binary.Write(&writer, binary.BigEndian, header)
	buff := writer.Bytes()
	for i := 0; i < len(buff); i += 2 {
		checksum.Add(binary.BigEndian.Uint16(buff[i : i+2]))
	}

	for i := 0; i < len(payload); i += 2 {
		checksum.Add(binary.BigEndian.Uint16(payload[i : i+2]))
	}

	header.Checksum = checksum.Compute()
}

func _parseIcmpEchoRequest(datagram []byte) *_icmpEchoRequest {

	var header _ipv4AndIcmpEchoHeader
	binary.Read(bytes.NewReader(datagram), binary.BigEndian, &header)

	if header.Ipv4.VersionAndIhl != 0x45 || header.Ipv4.Protocol != 1 ||
		header.IcmpEcho.Type != 8 || header.IcmpEcho.Code != 0 {
		return nil
	}

	return &_icmpEchoRequest{
		SourceAddress:      header.Ipv4.SourceAddress,
		DestinationAddress: header.Ipv4.DestinationAddress,
		Identifier:         header.IcmpEcho.Identifier,
		SequenceNumber:     header.IcmpEcho.SequenceNumber,
		Payload:            datagram[binary.Size(header):],
	}
}

func _createIcmpEchoResponse(request *_icmpEchoRequest) []byte {

	responseHeader := _ipv4AndIcmpEchoHeader{
		Ipv4: _ipv4Header{
			VersionAndIhl:          0x45,
			Tos:                    0,
			TotalLength:            0,
			Identification:         0, // TODO ?
			FlagsAndFragmentOffset: 0,
			Ttl:                    64,
			Protocol:               1,
			HeaderChecksum:         0,
			SourceAddress:          request.DestinationAddress,
			DestinationAddress:     request.SourceAddress,
		},
		IcmpEcho: _icmpEchoHeader{
			Type:           0,
			Code:           0,
			Identifier:     request.Identifier,
			SequenceNumber: request.SequenceNumber,
		},
	}
	datagramSize := binary.Size(responseHeader) + len(request.Payload)
	responseHeader.Ipv4.TotalLength = uint16(datagramSize)
	updateIpv4HeaderChecksum(&responseHeader.Ipv4)
	updateIcmpChecksum(&responseHeader.IcmpEcho, request.Payload)

	buff := bytes.Buffer{}
	binary.Write(&buff, binary.BigEndian, responseHeader)
	buff.Write(request.Payload)

	return buff.Bytes()
}

func (ping *_pingResponder) Write(p []byte) (int, error) {

	request := _parseIcmpEchoRequest(p)

	if request != nil {

		ping._closeMutex.Lock()
		defer ping._closeMutex.Unlock()

		if ping._isClosed {
			return 0, fmt.Errorf("object is closed")
		}

		ping._chan <- request
	}

	return len(p), nil
}

func (ping *_pingResponder) Read(p []byte) (int, error) {

	request, isStillOpen := <-ping._chan
	if !isStillOpen {
		return 0, io.EOF
	}

	response := _createIcmpEchoResponse(request)

	if len(response) > len(p) {
		return 0, fmt.Errorf("buffer is too small")
	}

	copy(p, response)

	fmt.Printf("Responding to ping request.\n")

	return len(response), nil
}

func (ping *_pingResponder) Close() error {

	ping._closeMutex.Lock()
	defer ping._closeMutex.Unlock()

	if ping._isClosed {
		return fmt.Errorf("already closed")
	}

	close(ping._chan)

	return nil
}

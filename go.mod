module tunme

go 1.17

require (
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1
	salsa.debian.org/vasudev/gospake2 v0.0.0-20210510093858-d91629950ad1
)

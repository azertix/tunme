BINARIES = tunme-cat tunme-relay tunme-test tunme-tun

.PHONY: all
all: ${BINARIES}

.PHONY: ${BINARIES}

tunme-cat:
	go build ./cmd/$@

tunme-relay:
	go build ./cmd/$@

tunme-test:
	go build ./cmd/$@

tunme-tun:
	go build ./cmd/$@

.PHONY: clean
clean:
	$(RM) ${BINARIES}

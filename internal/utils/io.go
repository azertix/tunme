package utils

import (
	"fmt"
	"io"
)

func ReadBytes(reader io.Reader, bytes []byte) (bool, error) {

	offset := 0

	for {

		n, err := reader.Read(bytes[offset:])

		offset += n

		if offset == len(bytes) {
			return true, err
		} else if err == io.EOF {
			return false, io.ErrUnexpectedEOF
		} else if err != nil {
			return false, err
		} else if n == 0 {
			return false, fmt.Errorf("read returned no data")
		}
	}
}

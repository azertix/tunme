package utils

const BuffLenBytes = 4
const BuffLenMax = int32(^uint32(0) >> 1)

// SerializeBuffLen serializes an integer which is supposed to represent the length
// of a buffer. To ensure interoperability, it is limited to what can be represented on
// a 31 bits integer.
func SerializeBuffLen(l int, out []byte) {

	if l > int(BuffLenMax) {
		panic("buffer is too large for its length to be serialized")
	}

	for i := BuffLenBytes - 1; i >= 0; i-- {
		out[i] = byte(l & 0xff)
		l >>= 8
	}
}

// ParseBuffLen performs the reverse operation of SerializeBuffLen.
func ParseBuffLen(in []byte) (int, error) {

	var l uint32

	for i := 0; i < BuffLenBytes; i++ {
		l <<= 8
		l |= uint32(in[i])
	}

	if l > uint32(BuffLenMax) {
		panic("buffer is too large for its length to be parsed")
	}

	return int(l), nil
}

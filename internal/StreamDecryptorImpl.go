package internal

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"fmt"
	"golang.org/x/crypto/blake2b"
	"hash"
	"io"
	"tunme/internal/utils"
	"tunme/pkg/tunme"
)

// TODO: handle EOF from the underlying stream as an error
// TODO: /!\ buffer size computation

type StreamDecryptorImpl struct {
	input io.Reader
	key   []byte

	// buffering
	buff     []byte
	buffOff  int
	buffLen  int
	isClosed bool // TODO: do not bufferize more if closed

	// cryptography
	cipher cipher.Stream
	hash   hash.Hash

	isFaulted bool
}

func CreateStreamDecryptorImpl(input io.Reader, bufferSize int, key []byte) *StreamDecryptorImpl {

	hashFunc, err := blake2b.New(32, key)
	if err != nil {
		panic(err)
	}

	return &StreamDecryptorImpl{
		input: input,
		key:   key,
		buff:  make([]byte, bufferSize),
		hash:  hashFunc,
	}
}

func (decryptor *StreamDecryptorImpl) Read(buf []byte) (int, error) {

	decryptor._ensureNotFaulted()

	if !decryptor.isClosed && decryptor.buffOff == decryptor.buffLen {
		err := decryptor._feedBuffer()
		if err != nil {
			return 0, decryptor._setFaulted(err)
		}
	}

	n := utils.Min(len(buf), decryptor.buffLen-decryptor.buffOff)

	copy(buf, decryptor.buff[decryptor.buffOff:decryptor.buffOff+n])
	decryptor.buffOff += n

	if decryptor.isClosed && decryptor.buffOff == decryptor.buffLen {
		return n, io.EOF
	} else {
		return n, nil
	}
}

func (decryptor *StreamDecryptorImpl) _feedBuffer() error {

	if decryptor.isClosed {
		return tunme.ClosedError
	}

	err := decryptor._ensureCipherInitialized()
	if err != nil {
		return err
	}

	// reading header

	var headerBuff [_streamCipherFixedHeaderLen]byte
	ok, err := utils.ReadBytes(decryptor.input, headerBuff[:])
	if !ok {
		return err
	}

	decryptor.buffLen = (int(headerBuff[0]) << 8) | int(headerBuff[1])
	if decryptor.buffLen > len(decryptor.buff) {
		return fmt.Errorf("authentication tag is too far away")
	}

	if headerBuff[2] != 0 {
		decryptor.isClosed = true
	}

	// reading ciphertext

	decryptor.buffOff = 0

	ok, err = utils.ReadBytes(decryptor.input, decryptor.buff[:decryptor.buffLen])
	if !ok {
		return err
	}

	// reading authentication tag

	tagBuff := make([]byte, decryptor.hash.Size())
	ok, err = utils.ReadBytes(decryptor.input, tagBuff)
	if !ok {
		return err
	}

	// checking authentication tag

	_, err = decryptor.hash.Write(decryptor.buff[:decryptor.buffLen])
	if err != nil {
		return err
	}

	if bytes.Compare(tagBuff, decryptor.hash.Sum(make([]byte, 0))) != 0 {
		return fmt.Errorf("stream authentication failed")
	}

	// decrypting ciphertext

	decryptor.cipher.XORKeyStream(decryptor.buff[:decryptor.buffLen], decryptor.buff[:decryptor.buffLen])

	//

	return nil
}

func (decryptor *StreamDecryptorImpl) _ensureCipherInitialized() error {

	if decryptor.cipher == nil {

		// instantiating block cipher

		blockCipher, err := aes.NewCipher(decryptor.key)
		if err != nil {
			return err
		}

		// reading nonce

		nonceBuff := make([]byte, blockCipher.BlockSize())
		ok, err := utils.ReadBytes(decryptor.input, nonceBuff)
		if !ok {
			return err
		}

		// instantiating stream cipher

		decryptor.cipher = cipher.NewOFB(blockCipher, nonceBuff)
	}

	return nil
}

func (decryptor *StreamDecryptorImpl) _setFaulted(err error) error {
	return err
}

func (decryptor *StreamDecryptorImpl) _ensureNotFaulted() {
	if decryptor.isFaulted {
		panic("trying to use a stream decryptor which is in a isFaulted state")
	}
}

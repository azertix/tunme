package tcp

import (
	"net"
	"sync"
	"tunme/pkg/tunme"
)

type serverStreamAcceptor interface {
	streamAcceptor
}

type _serverStreamAcceptor struct {
	_listener      *net.TCPListener
	_streamManager streamManager
	_chan          chan tunme.Stream
	_isClosed      bool
	_closeMutex    sync.Mutex
}

func createServerStreamAcceptor(listener *net.TCPListener, streamManager streamManager) serverStreamAcceptor {

	acc := &_serverStreamAcceptor{
		_listener:      listener,
		_streamManager: streamManager,
		_chan:          make(chan tunme.Stream),
	}

	go acc._loop()

	return acc
}

func (acc *_serverStreamAcceptor) Accept() (tunme.Stream, error) {

	stream, isStillOpen := <-acc._chan

	if !isStillOpen {
		return nil, tunme.ClosedError
	}

	return stream, nil
}

func (acc *_serverStreamAcceptor) Close() error {

	acc._closeMutex.Lock()
	defer acc._closeMutex.Unlock()

	if acc._isClosed {
		return tunme.ClosedError
	}

	acc._isClosed = true

	close(acc._chan)

	return nil
}

func (acc *_serverStreamAcceptor) _loop() {

	for acc._loopOnce() {
	}
}

func (acc *_serverStreamAcceptor) _loopOnce() bool {

	connection, err := acc._listener.AcceptTCP()
	if err != nil {
		return false
	}

	acc._closeMutex.Lock()
	defer acc._closeMutex.Unlock()

	if acc._isClosed {
		return false
	}

	stream, isNewStream := acc._streamManager.AttachTcpConnection(connection)
	if isNewStream {
		acc._chan <- stream
	}

	return true
}

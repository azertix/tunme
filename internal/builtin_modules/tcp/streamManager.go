package tcp

import (
	"fmt"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"
	"tunme/pkg/tunme"
)

type streamManager interface {
	io.Closer

	GetStream(id uint64) tunme.Stream

	// -- for client

	CreateClientStream(addr *net.TCPAddr) tunme.Stream

	CreateServerStreamFromClient(id uint64, addr *net.TCPAddr) tunme.Stream

	// -- for server

	// CreateServerStream creates a stream which should later be attached to a
	// TCP connection using AttachTcpConnection().
	CreateServerStream() tunme.Stream

	AttachTcpConnection(connection *net.TCPConn) (stream tunme.Stream, isNewStream bool)
}

type _streamManager struct {
	_services   tunme.Services
	_waitGroup  sync.WaitGroup
	_mutex      sync.Mutex
	_streams    map[uint64]streamImpl
	_closeOp    atomic.Value // 0 == none ; -1 == closing the manager ; n == closing n streams
	_idGenState uint64
}

func createStreamManager(services tunme.Services) streamManager {

	mgr := &_streamManager{
		_services: services,
		_streams:  map[uint64]streamImpl{},
	}

	mgr._closeOp.Store(0)

	return mgr
}

func (mgr *_streamManager) Close() error {

	// To avoid a deadlock, we must make sure that a stream is not currently closing.
	for !mgr._closeOp.CompareAndSwap(0, -1) {
		if mgr._closeOp.Load() == -1 {
			return fmt.Errorf("already closing")
		}
		time.Sleep(500 * time.Microsecond) // TODO
	}

	mgr._mutex.Lock()
	defer mgr._mutex.Unlock()

	for _, stream := range mgr._streams {
		stream.CloseImplicitly()
	}

	mgr._waitGroup.Wait()

	return nil
}

func (mgr *_streamManager) GetStream(id uint64) tunme.Stream {

	mgr._mutex.Lock()
	defer mgr._mutex.Unlock()

	return mgr._streams[id]
}

func (mgr *_streamManager) CreateClientStream(addr *net.TCPAddr) tunme.Stream {

	mgr._mutex.Lock()
	defer mgr._mutex.Unlock()

	mgr._waitGroup.Add(1)
	stream := createStreamWithConnection(
		mgr._createId(false),
		mgr._services,
		mgr._streamClosed,
		addr,
	)
	mgr._streams[stream.GetId()] = stream

	return stream
}

func (mgr *_streamManager) CreateServerStream() tunme.Stream {

	mgr._mutex.Lock()
	defer mgr._mutex.Unlock()

	mgr._waitGroup.Add(1)
	stream := createStream(
		mgr._createId(true),
		mgr._services,
		mgr._streamClosed,
	)
	mgr._streams[stream.GetId()] = stream

	return stream
}

func (mgr *_streamManager) AttachTcpConnection(connection *net.TCPConn) (tunme.Stream, bool) {

	mgr._mutex.Lock()
	defer mgr._mutex.Unlock()

	encryptedStream := createEncryptedStream(
		connection,
		mgr._services.CipherFactory().CreateStreamDecryptor(connection, maxSegmentSize),
		mgr._services.CipherFactory().CreateStreamEncryptor(connection, maxSegmentSize),
	)

	var streamHello streamHelloDto
	err := streamHello.read(encryptedStream)
	if err != nil {
		panic(err)
	}

	id := streamHello.streamId

	if mgr._isServerId(id) {
		// TODO: handle already closed stream
		stream := mgr._streams[id]
		stream.AttachTcpConnection(encryptedStream)
		return stream, false
	} else {
		mgr._waitGroup.Add(1)
		stream := createStream(
			id,
			mgr._services,
			mgr._streamClosed,
		)
		stream.AttachTcpConnection(encryptedStream)
		mgr._streams[stream.GetId()] = stream
		return stream, true
	}
}

func (mgr *_streamManager) CreateServerStreamFromClient(id uint64, addr *net.TCPAddr) tunme.Stream {

	mgr._waitGroup.Add(1)
	stream := createStreamWithConnection(
		id,
		mgr._services,
		mgr._streamClosed,
		addr,
	)
	mgr._streams[stream.GetId()] = stream

	return stream
}

func (mgr *_streamManager) _streamClosed(stream tunme.Stream) {

	defer mgr._waitGroup.Done()

	// To avoid a deadlock, we must make sure that the manager is not currently closing.
	if !mgr._registerStreamCloseOp() {
		return
	}
	defer mgr._unregisterStreamCloseOp()

	mgr._mutex.Lock()
	defer mgr._mutex.Unlock()

	delete(mgr._streams, stream.GetId())
}

func (mgr *_streamManager) _registerStreamCloseOp() bool {

	for {
		closeOp := mgr._closeOp.Load().(int)

		if closeOp == -1 {
			// The manager is currently closing.
			return false
		}

		if mgr._closeOp.CompareAndSwap(closeOp, closeOp+1) {
			return true
		}

		time.Sleep(500 * time.Microsecond)
	}
}

func (mgr *_streamManager) _unregisterStreamCloseOp() {

	for {
		closeOp := mgr._closeOp.Load().(int)

		if mgr._closeOp.CompareAndSwap(closeOp, closeOp-1) {
			break
		}

		time.Sleep(500 * time.Microsecond)
	}
}

func (mgr *_streamManager) _createId(isServer bool) uint64 {

	// TODO: handle overflow ?

	mgr._idGenState++

	id := mgr._idGenState * 2

	if isServer {
		id += 1
	}

	return id
}

func (mgr *_streamManager) _isServerId(id uint64) bool {
	return id%2 == 1
}

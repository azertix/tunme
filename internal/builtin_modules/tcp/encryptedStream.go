package tcp

import (
	"io"
	"net"
	"tunme/pkg/tunme"
)

// TODO: handle a Close() from the other side

type encryptedTcpStream interface {
	io.ReadWriteCloser
	Flush() error
}

type _encryptedTcpStream struct {
	_connection *net.TCPConn
	_input      tunme.StreamDecryptor
	_output     tunme.StreamEncryptor
}

func createEncryptedStream(connection *net.TCPConn, input tunme.StreamDecryptor, output tunme.StreamEncryptor) encryptedTcpStream {
	return &_encryptedTcpStream{
		_connection: connection,
		_input:      input,
		_output:     output,
	}
}

func (s *_encryptedTcpStream) Read(p []byte) (int, error) {
	return s._input.Read(p)
}

func (s *_encryptedTcpStream) Write(p []byte) (int, error) {
	return s._output.Write(p)
}

func (s *_encryptedTcpStream) Close() error {

	// TODO: ensure every Read() and Write() is terminated (Reads might currently not be affected)

	var err error

	err = s._output.Close()
	if err != nil {
		panic(err) // TODO
	}

	err = s._connection.Close()
	if err != nil {
		panic(err) // TODO
	}

	return nil
}

func (s *_encryptedTcpStream) Flush() error {
	return s._output.Flush()
}

package tcp

import (
	"bytes"
	"io"
	"net"
	"tunme/pkg/tunme"
)

// TODO: remove panic()
// TODO: handle fatal errors
// TODO: concurrency

// TODO
const controlMsgBuffSize = 1000

type controlMsg struct {
	t    controlMsgType
	data interface{}
}

// TODO: use the lifecycle manager
type controlStream interface {
	SendDatagram(payload []byte) error
	SendOpenStream(id uint64)
	Receive() (*controlMsg, error)
	io.Closer
}

type _controlStream struct {
	_stream    encryptedTcpStream
	_lifecycle closerLifecycle
}

func createControlStream(connection *net.TCPConn, services tunme.Services) controlStream {
	return &_controlStream{
		_stream: createEncryptedStream(
			connection,
			services.CipherFactory().CreateStreamDecryptor(connection, controlMsgBuffSize),
			services.CipherFactory().CreateStreamEncryptor(connection, controlMsgBuffSize),
		),
		_lifecycle: createCloserLifecycle(0), // TODO: add operations
	}
}

func (ctrl *_controlStream) SendDatagram(payload []byte) error {

	err := ctrl._writeMsgType(controlMsgTypeDatagram)
	if err != nil {
		return err
	}

	msg := controlMsgDatagramDto{
		datagram: payload,
	}
	err = msg.write(ctrl._stream)
	if err != nil {
		return err
	}

	err = ctrl._stream.Flush()
	if err != nil {
		return err
	}

	return nil
}

func (ctrl *_controlStream) SendOpenStream(id uint64) {

	err := ctrl._writeMsgType(controlMsgTypeOpenStream)
	if err != nil {
		panic(err)
	}

	msg := controlMsgOpenStreamDto{
		streamId: id,
	}
	err = msg.write(ctrl._stream)
	if err != nil {
		panic(err)
	}

	err = ctrl._stream.Flush()
	if err != nil {
		panic(err)
	}
}

func (ctrl *_controlStream) Receive() (*controlMsg, error) {

	controlMsgType, err := readControlMsgType(ctrl._stream)
	if err != nil {
		return nil, err
	}

	msg := controlMsg{
		t: controlMsgType,
	}

	if controlMsgType == controlMsgTypeDatagram {
		dto := controlMsgDatagramDto{}
		err = dto.read(ctrl._stream)
		if err != nil {
			panic(err)
		}
		msg.data = dto
	} else if controlMsgType == controlMsgTypeOpenStream {
		dto := controlMsgOpenStreamDto{}
		err = dto.read(ctrl._stream)
		if err != nil {
			panic(err)
		}
		msg.data = dto
	} else {
		panic("invalid control message type")
	}

	return &msg, nil
}

func (ctrl *_controlStream) Close() error {

	shouldClose, err := ctrl._lifecycle.CloseStarted(true)
	if !shouldClose {
		return err
	}
	defer ctrl._lifecycle.CloseFinished()

	return ctrl._stream.Close()
}

func (ctrl *_controlStream) _writeMsgType(t controlMsgType) error {

	var msgTypeBuff [1]byte
	msgTypeBuff[0] = byte(t)

	_, err := ctrl._stream.Write(msgTypeBuff[:])
	return err
}

func (ctrl *_controlStream) _decodeMsg(msg []byte) *controlMsg {

	reader := bytes.NewReader(msg)
	controlMsgType, err := readControlMsgType(reader)
	if err != nil {
		panic("invalid message type")
	}

	decoded := controlMsg{
		t: controlMsgType,
	}

	if controlMsgType == controlMsgTypeDatagram {
		decoded.data = msg[1:] // TODO: use reader
	} else if controlMsgType == controlMsgTypeOpenStream {
		var dto controlMsgOpenStreamDto
		dto.read(reader)
		decoded.data = dto
	}

	return &decoded
}

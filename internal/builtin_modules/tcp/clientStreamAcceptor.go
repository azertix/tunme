package tcp

import (
	"sync"
	"tunme/pkg/tunme"
)

type clientStreamAcceptor interface {
	streamAcceptor
	StreamRequestReceived(stream tunme.Stream)
}

type _clientStreamAcceptor struct {
	_chan       chan tunme.Stream
	_isClosed   bool
	_closeMutex sync.Mutex
}

func createClientStreamAcceptor() clientStreamAcceptor {
	return &_clientStreamAcceptor{
		_chan: make(chan tunme.Stream),
	}
}

func (acc *_clientStreamAcceptor) Accept() (tunme.Stream, error) {

	stream, isStillOpen := <-acc._chan

	if !isStillOpen {
		return nil, tunme.ClosedError
	}

	return stream, nil
}

func (acc *_clientStreamAcceptor) Close() error {

	acc._closeMutex.Lock()
	defer acc._closeMutex.Unlock()

	if acc._isClosed {
		return tunme.ClosedError
	}

	acc._isClosed = true

	close(acc._chan)

	return nil
}

func (acc *_clientStreamAcceptor) StreamRequestReceived(stream tunme.Stream) {

	acc._closeMutex.Lock()
	defer acc._closeMutex.Unlock()

	if !acc._isClosed {
		acc._chan <- stream
	}
}

package tunme_app

import (
	"fmt"
	"tunme/internal/builtin_modules/tcp"
	"tunme/pkg/tunme"
)

type App struct {
	// TODO: use a simple UUID mechanism to ensure both ends are the same app
	_cipherFactory tunme.CipherFactory
}

func (app *App) SetCipherFactory(cipherFactory tunme.CipherFactory) {
	app._cipherFactory = cipherFactory
}

func (app *App) CipherFactory() tunme.CipherFactory {
	return app._cipherFactory
}

func (app *App) CreateLink(module string, args []string) (tunme.Link, error) {

	var mod tunme.Module
	var err error

	err = app._fillDependencies()
	if err != nil {
		return nil, err
	}

	if module == "tcp" {
		mod = &tcp.Module{}
	} else {
		return nil, fmt.Errorf("unknown module %s", module)
	}

	link, err := mod.CreateLink(args, app)
	if err != nil {
		return nil, err
	}

	return link, err
}

func (app *App) _fillDependencies() error {

	if app._cipherFactory == nil {
		app._cipherFactory = CreatePasswordCipherFactory()
	}

	return nil
}

package tunme

import (
	"io"
)

// StreamEncryptor
// Closing a StreamEncryptor shall not close the underlying stream. After Close() returned, the underlying stream
// will not be used by the StreamEncryptor.
type StreamEncryptor interface {
	io.WriteCloser
	Flush() error
}

type StreamDecryptor interface {
	io.Reader
}

type CipherFactory interface {

	IsHandshakeFinished() bool

	// NextHandshakeStep performs a step of the handshake. The parameter can be null
	// for the first step on the client.
	NextHandshakeStep(received []byte) ([]byte, error)

	// In the following functions, bufferSize determines how many bytes max will be written at once to the output.
	// Ideally, the implementation will avoid making writes a lot shorter than that size.
	// This parameter must match on both ends.

	CreateStreamEncryptor(output io.Writer, bufferSize int) StreamEncryptor

	CreateStreamDecryptor(input io.Reader, bufferSize int) StreamDecryptor
}
